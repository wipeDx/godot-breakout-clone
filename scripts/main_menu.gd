extends CanvasLayer

onready var menu = get_node("menu")

const MENU_OPTION_LOAD_STAGE = 0
const MENU_OPTION_STAGE_EDITOR = 1
const MENU_OPTION_OPTIONS = 2
const MENU_OPTION_QUIT_GAME = 3

func _ready():
	menu.connect("option_selected", self, "_on_option_selected")
	pass

func _on_option_selected(option):
	if option == MENU_OPTION_LOAD_STAGE:
		stage_manager.change_stage(stage_manager.STAGE_GAME)
		pass
	elif option == MENU_OPTION_STAGE_EDITOR:
		stage_manager.change_stage(stage_manager.STAGE_EDITOR)
		pass
	elif option == MENU_OPTION_OPTIONS:
		pass
	elif option == MENU_OPTION_QUIT_GAME:
		get_tree().quit()
		pass