extends Node2D

var brick_script

var speed = 500
var ball_speed = 500
var ball_change_angle_size = 50
var screen_size
var pad_size

var ball_moving
var ball_direction
var ball_size
var ball_pos

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	brick_script = get_node("/root/Game/Bricks")
	screen_size = get_viewport_rect().size
	pad_size = get_node("Paddle").get_texture().get_size()/2 * get_node("Paddle").get_scale()
	ball_moving = false;
	ball_direction = Vector2(0, -1)
	ball_size = get_node("Ball/Sprite").get_texture().get_size()/2 * get_node("Ball").get_scale()
	
	set_process(true)
	set_process_input(true)
	pass

func _process(delta):
	var pos = get_node("Paddle").get_pos()
	ball_pos = get_node("Ball").get_pos()

	#Input left
	if (pos.x > -screen_size.x/2 + pad_size.x and Input.is_action_pressed("move_left")):
		pos.x += -speed * delta
		#Move ball with paddle
		if (!ball_moving):
			ball_pos.x += -speed * delta

	#Input right
	if (pos.x < screen_size.x/2 - pad_size.x and Input.is_action_pressed("move_right")):
		pos.x += speed * delta
		#Move ball with paddle
		if (!ball_moving):
			ball_pos.x += speed * delta
	get_node("Paddle").set_pos(pos)
	
	#Release the ball!
	if (!ball_moving and Input.is_action_pressed("release_ball")):
		print("Ball is moving!")
		ball_moving = true
	
	#Ball change directions bouncing on the Up Side
	if (ball_pos.y <= -(screen_size.y/2 - ball_size.y) or (ball_pos.y >= 235 and ball_pos.x >= (pos.x - pad_size.x - ball_size.x) and ball_pos.x <= (pos.x + pad_size.x + ball_size.x))):
		ball_direction.y *= -1

	#Change ball angle on left side
	if (ball_pos.y >= 235 and ball_pos.y <= 240 and (ball_is_between(pos.x - pad_size.x, pos.x - pad_size.x + ball_change_angle_size))):
		if (ball_direction.x <= -1):
			ball_direction.x = ball_direction.x - 0.3
		else:
			ball_direction.x = -0.9

	#Change ball angle on left side
	if (ball_pos.y >= 235 and ball_pos.y <= 240 and (ball_is_between(pos.x + pad_size.x - ball_change_angle_size, pos.x + pad_size.x))):
		if (ball_direction.x <= 1):
			ball_direction.x = ball_direction.x + 0.3
		else:
			ball_direction.x = 0.9
	
	#Check if ball intersects with a brick
	for brick in brick_script.getBricks():
		if (ball_intersects(brick)):
			print("LUL")
			ball_direction.y *= -1

	#Ball change directions bouncing on the Left/Right side
	if (ball_pos.x <= -screen_size.x/2 + ball_size.x or ball_pos.x >= screen_size.x/2 - ball_size.x):
		ball_direction.x *= -1
	
	if (ball_pos.y >= screen_size.y/2 + ball_size.x):
		ball_moving = false
		get_node("/root/Game/GameLostText").show()
		set_process(false)
	
	if (ball_moving):
		ball_pos += ball_direction * delta * ball_speed
	
	get_node("Ball").set_pos(ball_pos)
	debug(ball_pos, pos)
	pass
	
func _input(event):
	if (event.is_action_pressed("pause")):
		set_process(!is_processing())
	if (!is_processing() and event.is_action_pressed("reset")):
		get_tree().change_scene("res://Scenes/Game.tscn")
	pass

func ball_is_between(left, right):
	return ball_pos.x >= left and ball_pos.x <= right
	pass

func ball_intersects(object):
	if (false):
		brick_script.getBricks().erase(object)
		object.remove_and_skip()
		return true
	else:
		return false
	

func debug(ballpos, paddlepos):
	var BallPos = get_node("/root/Game/DEBUG/BallPos")
	var PaddlePos = get_node("/root/Game/DEBUG/PaddlePos")
	BallPos.set_bbcode("(" + str(ballpos.x) + "|" + str(ballpos.y) + ") Ball")
	PaddlePos.set_bbcode("(" + str(paddlepos.x) + "|" + str(paddlepos.y) + ") Paddle")

func _on_Area2D_body_enter_shape( body_id, body, body_shape, area_shape ):
	print("test")
	pass # replace with function body
