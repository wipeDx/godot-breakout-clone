extends Node

signal brick_cycle_next
signal brick_cycle_prev
signal ball_paddle_toggle
signal place_brick
signal toggle_grid

func _ready():
	set_process_input(true)
	get_node("ball_paddle/paddle/sprite_line").queue_free()
	pass

func _input(event):
	if event.is_echo(): return
	if event.is_action_pressed("stage_editor_next_brick_color"):
		emit_signal("brick_cycle_next")
	if event.is_action_pressed("stage_editor_prev_brick_color"):
		emit_signal("brick_cycle_next")
	if event.is_action_pressed("stage_editor_toggle_ball_paddle"):
		emit_signal("ball_paddle_toggle")
	if event.is_action_pressed("stage_editor_toggle_grid"):
		emit_signal("toggle_grid")
	if event.is_action_pressed("stage_editor_place_brick"):
		emit_signal("place_brick")
	if event.is_action_pressed("ui_cancel"):
		toggle_escape_menu()
	pass

func toggle_escape_menu():
	var esc_menu = find_node("cntr_esc_menu")
	
	if esc_menu.is_hidden():
		find_node("spr_brick_mouse").set_process(false)
		esc_menu.show()
	else:
		find_node("spr_brick_mouse").set_process(true)
		esc_menu.hide()