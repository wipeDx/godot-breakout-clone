extends RigidBody2D

onready var game = get_parent()
onready var speed = 500
var added_speed = 0

signal game_over
signal brick_destroyed
signal hit_paddle

func _ready():
	connect("body_enter", self, "_on_body_enter")
	pass

func _on_body_enter(other):
	if other.is_in_group(utils.GROUP_PADDLE) and added_speed < 800:
		var angle = Vector2(sin(get_applied_force().x), cos(get_applied_force().y))
		apply_impulse(Vector2(0,0), -angle * 15)
		added_speed += 15
		emit_signal("hit_paddle")
	elif other.is_in_group(utils.GROUP_GAMEOVER):
		set_linear_velocity(Vector2(0,0))
		emit_signal("game_over")
	elif other.is_in_group(utils.GROUP_BRICKS):
		other.queue_free()
		emit_signal("brick_destroyed")