extends Node

onready var scn_brick = preload("res://scenes/brick.tscn")


const bricks_to_generate = 210
const offset_side = 32

const BRICK_COLOR_BLUE   = 0
const BRICK_COLOR_RED    = 1
const BRICK_COLOR_GREEN  = 2
const BRICK_COLOR_PURPLE = 3
const BRICK_COLOR_YELLOW = 4

const SPAWNER_TYPE_0 = 0
const SPAWNER_TYPE_1 = 1
const SPAWNER_TYPE_CUSTOM = 2

var max_bricks_per_row
var current_pos = Vector2(offset_side, offset_side/2 + utils.UI_OFFSET_TOP)
var current_brick_color = BRICK_COLOR_RED


func _ready():
	calculate_variables()
	for i in range(bricks_to_generate):
		spawn_brick()
		move_pos(i+1)
	pass

func switch_color():
	if current_brick_color == BRICK_COLOR_RED: current_brick_color = BRICK_COLOR_YELLOW
	elif current_brick_color == BRICK_COLOR_YELLOW: current_brick_color = BRICK_COLOR_GREEN
	elif current_brick_color == BRICK_COLOR_GREEN: current_brick_color = BRICK_COLOR_BLUE
	elif current_brick_color == BRICK_COLOR_BLUE: current_brick_color = BRICK_COLOR_PURPLE
	else: current_brick_color = BRICK_COLOR_RED
	pass

func spawn_brick():
	var new_brick = scn_brick.instance()
	print (current_pos)
	new_brick.set_pos(current_pos)
	new_brick.add_to_group(utils.GROUP_BRICKS)
	new_brick.get_node("sprite").set_texture(get_brick_color_texture())
	add_child(new_brick)
	pass

func move_pos(i):
	current_pos.x += 64
	if i > 0 and i % max_bricks_per_row == 0:
		current_pos.x = offset_side
		current_pos.y += 16
		switch_color()
	pass

func get_brick_color_texture():
	if current_brick_color == BRICK_COLOR_BLUE: return load("res://sprites/Block_b.png")
	elif current_brick_color == BRICK_COLOR_RED: return load("res://sprites/Block_r.png")
	elif current_brick_color == BRICK_COLOR_GREEN: return load("res://sprites/Block_g.png")
	elif current_brick_color == BRICK_COLOR_PURPLE: return load("res://sprites/Block_p.png")
	else: return load("res://sprites/Block_y.png")
	pass

func calculate_variables():
	var temp_brick = scn_brick.instance()
	var spr_brick = temp_brick.get_node("sprite")
	var size_brick = spr_brick.get_texture().get_size() * spr_brick.get_scale()
	
	max_bricks_per_row = floor((utils.GAME_BOUNDARY_RIGHT - offset_side) / size_brick.x)
	max_bricks_per_row = int(max_bricks_per_row)
	print(max_bricks_per_row)
	pass
