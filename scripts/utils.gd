extends Node

const GAME_BOUNDARY_LEFT = 0
const GAME_BOUNDARY_RIGHT = 1024
const UI_OFFSET_TOP = 32

const GROUP_BALL     = "ball"
const GROUP_PADDLE   = "paddle"
const GROUP_BRICKS   = "bricks"
const GROUP_GAMEOVER = "gameover"

func _ready():
	pass

func get_main_node():
	var root = get_tree().get_root()
	return root.get_child(root.get_child_count()-1)
	pass

func in_between(x, var1, var2):
	return x > var1 and x < var2
	pass