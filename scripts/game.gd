extends Node2D

onready var paddle = utils.get_main_node().get_node("paddle")
onready var ball = utils.get_main_node().get_node("ball")

const SCORE_PER_BRICK = 100

var score = 0
var bonus = 1
var bricks_until_bonus = 5

signal score_changed
signal bonus_changed

func _ready():
	paddle.ball_ready(ball)
	add_to_group(utils.GROUP_BALL)
	ball.connect("game_over", self, "_on_game_over")
	ball.connect("brick_destroyed", self, "_on_add_score")
	ball.connect("hit_paddle", self, "_on_reset_bonus")
	pass

func _on_game_over():
	_on_reset_bonus()
	paddle.set_process(false)
	pass

func _on_add_score():
	manage_bonus()
	score += SCORE_PER_BRICK * bonus
	print(bonus)
	emit_signal("score_changed", score)
	pass

func manage_bonus():
	bricks_until_bonus = max(0, bricks_until_bonus - 1)
	if bricks_until_bonus == 0 and bonus < 10:
		bonus += 1
		emit_signal("bonus_changed", bonus)
	pass

func _on_reset_bonus():
	bricks_until_bonus = 2
	bonus = 1
	emit_signal("bonus_changed", bonus)
	pass