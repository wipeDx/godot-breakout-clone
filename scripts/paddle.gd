extends RigidBody2D

const paddle_speed = 500
const line_speed = 1

onready var spr_paddle = get_node("sprite_paddle")
onready var spr_line = get_node("sprite_line")

var ball


func _ready():
	set_process(true)
	set_fixed_process(true)
	set_process_input(true)
	
	spr_line.set_rotd(randi()%121-60)
	
	add_to_group(utils.GROUP_PADDLE)
	set_contact_monitor( true )
	
	pass

func _process(delta):
	if Input.is_action_pressed("move_left"):
		process_paddle(-1 * paddle_speed * delta)
		
	
	if Input.is_action_pressed("move_right"):
		process_paddle(paddle_speed * delta)
	
	if Input.is_action_pressed("move_line_left"):
		process_line(line_speed * delta)
	
	if Input.is_action_pressed("move_line_right"):
		process_line(-1 * line_speed * delta)
	
	pass

func _input(event):
	
	if event.is_action_pressed("release_ball"):
		release_ball()
	
	if event.is_action_pressed("reset"):
		get_tree().reload_current_scene()
	pass

func process_paddle(direction):
	var current_pos = get_pos()
	if current_pos.y != 550:
		current_pos.y = 550
	if direction < 0 and current_pos.x > utils.GAME_BOUNDARY_LEFT + get_paddle_size() or direction > 0 and current_pos.x < utils.GAME_BOUNDARY_RIGHT - get_paddle_size():
		current_pos.x += direction
		process_ball(direction)
		set_pos(current_pos)
	else:
		if direction < 0:
			set_pos(Vector2(get_paddle_size(), current_pos.y))
		else:
			set_pos(Vector2(utils.GAME_BOUNDARY_RIGHT - get_paddle_size(), current_pos.y))
	pass

func process_line(direction):
	var current_rot = spr_line.get_rot()
	if direction < 0 and current_rot > deg2rad(-60) or direction > 0 and current_rot < deg2rad(60):
		spr_line.set_rotd(rad2deg(spr_line.get_rot() + direction))
	else:
		if direction < 0:
			spr_line.set_rotd(-60)
		else:
			spr_line.set_rotd(60)
	pass

func get_paddle_size():
	return spr_paddle.get_texture().get_width() * spr_paddle.get_scale().x / 2

func process_ball(direction):
	if ball:
		var current_pos = ball.get_pos()
		current_pos.x += direction
		ball.set_pos(current_pos)
	pass

func release_ball():
	if ball:
		var angle = Vector2(sin(spr_line.get_rot()), cos(spr_line.get_rot()))
		ball.apply_impulse(Vector2(0,0), -angle * ball.speed)
		ball = null
	get_node("sprite_line").hide()
	pass

func ball_ready(ball):
	self.ball = ball
	pass

func _fixed_process(delta):
	pass