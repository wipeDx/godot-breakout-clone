extends CanvasLayer

const STAGE_GAME = "res://stages/game_stage.tscn"
const STAGE_MENU = "res://stages/main_menu.tscn"
const STAGE_EDITOR = "res://stages/stage_editor.tscn"

var is_changing = false

signal stage_changed

func _ready():
	pass

func change_stage(stage_path):
	if is_changing: return
	
	is_changing = true
	get_tree().get_root().set_disable_input(true)
	
	var anim = get_node("anim")
	#fade to black
	anim.play("fade_in")
	#sfx_swooshing
	#audio_player.play(game.sound_skin[3])
	yield(anim, "finished")
	#change stage
	get_tree().change_scene(stage_path)
	emit_signal("stage_changed")
	
	#fade from black
	anim.play("fade_out")
	yield(anim, "finished")
	
	is_changing = false
	get_tree().get_root().set_disable_input(false)